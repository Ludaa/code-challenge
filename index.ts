import { AbsolutePath, Path, User } from './src/types';

/**
 * Create a list of all paths and their minimum access level
 * @returns {Array<Object>} modified registry
 * @param registry
 */
export const getAllPaths = (registry: Path[]): AbsolutePath[] => {
  const getAbsolutePath = (registry: Path[], path: Path | null): string => {
    if (!path.parent){
      return '';
    }

    const parent = registry.find((current) => current.path === path.parent);
    return `${getAbsolutePath(registry, parent)}${path.path}`;
  }

  return registry.map((path) => !path.parent
      ? ({...path, absolutePath: path.path})
      : ({...path, absolutePath: getAbsolutePath(registry, path)}));
}

/**
 * Check accessibility for a user
 * @returns {Boolean} if the user has access
 * @param user User
 * @param path string
 * @param absolutePaths AbsolutePath[]
 */
export const hasAccess = (user: User, path: string, absolutePaths: AbsolutePath[]) => {
  const absolutePath = absolutePaths.find((absolutePath) => absolutePath.absolutePath === path);
  if (!absolutePath){
    console.info('Path not found!');
    return false;
  }

  const spittedPaths = path.split('/').filter(Boolean);
  const subPaths = spittedPaths.reduce((acc, curr, i) => {
    if (i === 0) {
      return [...acc, `/${curr}`];
    }

    return [...acc, `${acc[i]}/${curr}`];
  }, ['/']);

  return subPaths.every((subPath) => {
    const foundPath = absolutePaths.find((absolutePath) => absolutePath.absolutePath === subPath);
    return foundPath && foundPath.level <= user.level;
  });
}

/**
 * Get all paths a user has access too
 * @returns {Array<Object>} filtered array of routes
 * @param user User
 * @param paths AbsolutePath
 */
export const getUserPaths = (user: User, paths: AbsolutePath[]) => {
  return paths.filter((path) => hasAccess(user, path.absolutePath, paths));
}
