export interface Path {
    path: string;
    parent: string| null;
    level: number;
}

export interface AbsolutePath extends Path{
    absolutePath: string;
}
